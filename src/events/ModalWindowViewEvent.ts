export default class ModalWindowViewEvent extends CustomEvent<any> {
    public static readonly OPEN_EVENT = 'OPEN_EVENT';
    public static readonly CLOSE_EVENT = 'CLOSE_EVENT';
    public static readonly CLOSE_ANIMATION_END_EVENT = 'CLOSE_ANIMATION_END_EVENT';
    public static readonly OPEN_ANIMATION_END_EVENT = 'OPEN_ANIMATION_END_EVENT';

    public constructor(name: string) {
        super(name);
    }
}