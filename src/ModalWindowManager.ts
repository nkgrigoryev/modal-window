import ModalWindowView from './ModalWindowView';

export default class ModalWindowManager {
    public static readonly KEY_DOWN_JS_EVENT = 'keydown';
    public static readonly ESCAPE_KEY = 'Escape';

    private static _instance: ModalWindowManager;

    private _opened: ModalWindowView[];
    private _body: HTMLElement;
    private _container: HTMLElement;

    public static get instance(): ModalWindowManager {
        if (!ModalWindowManager._instance) {
            ModalWindowManager._instance = new ModalWindowManager();
        }
        return ModalWindowManager._instance;
    }

    private constructor() {
        if (ModalWindowManager._instance) {
            throw new Error('Use ModalWindowManager.instance');
        }
        this._init();
    }

    private _init(): void {
        this._opened = [];
        this._body = document.body;
        this._container = document.createElement('div');
        this._container.style.display = 'none';
        this._body.appendChild(this._container);
        document.addEventListener(ModalWindowManager.KEY_DOWN_JS_EVENT, this._onDocumentKeyDown.bind(this));
    }

    private _onDocumentKeyDown(event: KeyboardEvent): void {
        if (event.key !== ModalWindowManager.ESCAPE_KEY) return;
        if (this._opened.length) {
            let lastOpened = this._opened[this._opened.length - 1];
            lastOpened.close();
        }
    }

    public open(modalWindowView: ModalWindowView): void {
        if (!this._opened.length) {
            let scrollBarPadding = window.innerWidth - document.documentElement.clientWidth;
            this._body.style.width = `calc(100% - ${scrollBarPadding}px)`;
            this._body.style.overflow = 'hidden';
            this._body.style.position = 'relative';
            this._container.style.display = 'block';
        }
        this._opened.push(modalWindowView);
    }

    public close(modalWindowView: ModalWindowView): void {
        let index = this._opened.indexOf(modalWindowView);
        this._opened.splice(index, 1);
        if (!this._opened.length) {
            this._body.style.width = '';
            this._body.style.overflow = 'auto';
            this._body.style.position = '';
            this._container.style.display = 'none';
        }
    }

    public get container(): HTMLElement {
        return this._container;
    }
}