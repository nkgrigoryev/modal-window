import './ModalWindowView.css';
import createElementFromHTML from '@casino/create-element-from-html';
import EventDispatcher from '@casino/event-dispatcher';
import {TweenMax} from 'gsap';
import ModalWindowManager from './ModalWindowManager';
import ModalWindowViewEvent from './events/ModalWindowViewEvent';
import Tween = GSAPStatic.Tween;

export default class ModalWindowView extends EventDispatcher {
    private static readonly _EASE: string = 'power1.in';
    private static readonly _SHOW_ANIMATION_DURATION: number = 0.3;
    private static readonly _HIDE_ANIMATION_DURATION: number = 0.2;
    private static readonly _Y_ANIMATION_SHIFT: number = 50;

    private _isShowed: Boolean;
    private _manager: ModalWindowManager;

    private readonly _container: HTMLElement;
    private readonly _vail: HTMLElement;
    private readonly _rectangle: HTMLElement;
    private readonly _box: HTMLElement;
    private readonly _boxBackground: HTMLElement;
    private readonly _content: HTMLElement;
    private readonly _buttons: HTMLElement;

    private _target: HTMLElement;
    private _rectangleAnimation: Tween;
    private _vailAnimation: Tween;

    constructor() {
        super();
        this._isShowed = false;
        this._manager = ModalWindowManager.instance;

        this._container = createElementFromHTML(
            `<div class="ModalWindowView">
    <div class="ModalWindowView__fixedFullscreen ModalWindowView__vail vail-js"></div>
    <div class="ModalWindowView__background ModalWindowView__rectangle rectangle-js"></div>
    <div class="ModalWindowView__fixedFullscreen ModalWindowView__box box-js">
        <div class="ModalWindowView__background background-js">
            <div class="ModalWindowView__boxContent content-js">Text</div>
            <div class="ModalWindowView__boxButtons buttons-js"></div>
        </div>
    </div>
</div>`);
        this._manager.container.appendChild(this._container);
        this._vail = this._container.querySelector('.vail-js') as HTMLElement;
        this._vail.addEventListener('click', this.close.bind(this));

        this._rectangle = this._container.querySelector('.rectangle-js') as HTMLElement;
        this._rectangle.addEventListener('click', this.close.bind(this));

        this._box = this._container.querySelector('.box-js') as HTMLElement;
        this._boxBackground = this._box.querySelector('.background-js') as HTMLElement;
        this._content = this._box.querySelector('.content-js') as HTMLElement;
        this._buttons = this._box.querySelector('.buttons-js') as HTMLElement;
    }

    public open(target: HTMLElement) {
        if (this._isShowed) return;
        this._isShowed = true;
        this._killAllAnimations();
        this._manager.open(this);
        this._target = target;
        this._vail.style.display = 'block';
        this._box.style.display = 'block';
        this._box.style.opacity = '0';
        this._rectangle.style.display = 'block';
        let positions = this._getPositions();

        this._rectangle.style.left = positions.closedX;
        this._rectangle.style.top = positions.closedY;
        this._rectangle.style.width = positions.closedWidth;
        this._rectangle.style.height = positions.closedHeight;
        this._rectangle.style.opacity = '0.4';

        this._rectangleAnimation = TweenMax.to(this._rectangle, ModalWindowView._SHOW_ANIMATION_DURATION, {
            left: positions.openedX,
            top: positions.openedY,
            width: positions.openedWidth,
            height: positions.openedHeight,
            opacity: '1',
            ease: ModalWindowView._EASE,
            onComplete: () => {
                this._rectangle.style.display = 'none';
                this._box.style.opacity = '1';
                this.dispatchEvent(new ModalWindowViewEvent(ModalWindowViewEvent.OPEN_ANIMATION_END_EVENT));
            }
        });

        this._vail.style.opacity = '0';
        this._vailAnimation = TweenMax.to(this._vail, ModalWindowView._SHOW_ANIMATION_DURATION, {
            opacity: '1'
        });
        this.dispatchEvent(new ModalWindowViewEvent(ModalWindowViewEvent.OPEN_EVENT));
    }

    public close(): void {
        if (!this._isShowed) return;
        this._isShowed = false;
        this._killAllAnimations();
        let positions = this._getPositions();

        this._rectangleAnimation = TweenMax.to(this._rectangle, ModalWindowView._HIDE_ANIMATION_DURATION, {
            left: positions.closedX,
            top: positions.closedY,
            width: positions.closedWidth,
            height: positions.closedHeight,
            opacity: '0',
            onComplete: () => {
                this._manager.close(this);
                this._vail.style.display = 'none';
                this.dispatchEvent(new ModalWindowViewEvent(ModalWindowViewEvent.CLOSE_ANIMATION_END_EVENT));
            }
        });

        this._vailAnimation = TweenMax.to(this._vail, ModalWindowView._HIDE_ANIMATION_DURATION, {
            opacity: '0'
        });

        this._rectangle.style.display = 'block';
        this._box.style.display = 'none';
        this.dispatchEvent(new ModalWindowViewEvent(ModalWindowViewEvent.CLOSE_EVENT));
    }

    /**
     * @return {Object} Example:
     *     {
     *         closedX: 10px,
     *         closedY: 10px,
     *         closedWidth: 10px,
     *         closedHeight: 10px,
     *         openedX: 10px,
     *         openedY: 10px,
     *         openedWidth: 10px,
     *         openedHeight: 10px
     *     }
     * @private
     */
    private _getPositions(): PositionsInterface {
        let boxBackgroundRectangle = this._boxBackground.getBoundingClientRect();
        let openedX = boxBackgroundRectangle.left;
        let openedY = boxBackgroundRectangle.top;
        let openedWidth = this._boxBackground.offsetWidth;
        let openedHeight = this._boxBackground.offsetHeight;

        let closedX = openedX;
        let closedY = openedY - ModalWindowView._Y_ANIMATION_SHIFT;
        let closedWidth = openedWidth;
        let closedHeight = openedHeight;

        if (this._target &&
            this._target.getBoundingClientRect() &&
            this._target.offsetWidth > 1 &&
            this._target.offsetHeight > 1) {
            let targetRectangle = this._target.getBoundingClientRect();
            closedX = targetRectangle.left;
            closedY = targetRectangle.top;
            closedWidth = this._target.offsetWidth;
            closedHeight = this._target.offsetHeight;
        }

        return {
            openedX: `${openedX}px`,
            openedY: `${openedY}px`,
            openedWidth: `${openedWidth}px`,
            openedHeight: `${openedHeight}px`,
            closedX: `${closedX}px`,
            closedY: `${closedY}px`,
            closedWidth: `${closedWidth}px`,
            closedHeight: `${closedHeight}px`
        }
    }

    private _killAllAnimations(): void {
        if (this._rectangleAnimation) this._rectangleAnimation.kill();
        if (this._vailAnimation) this._rectangleAnimation.kill();
    }

    public get content(): HTMLElement {
        return this._content;
    }

    public get buttons(): HTMLElement {
        return this._buttons;
    }
}

interface PositionsInterface {
    closedX: string;
    closedY: string;
    closedWidth: string;
    closedHeight: string;
    openedX: string;
    openedY: string;
    openedWidth: string;
    openedHeight: string;
}