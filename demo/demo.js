import '@casino/demo-css'
import {ModalWindowView, ModalWindowViewEvent} from "../";

let modalWindow1 = new ModalWindowView();
modalWindow1.content.style.lineHeight = '1.5';
modalWindow1.content.innerHTML = 'First';
modalWindow1.buttons.innerHTML = '<button id="close-1-js">CLOSE</button>';

let modalWindow2 = new ModalWindowView();
modalWindow2.content.style.lineHeight = '1.5';
modalWindow2.content.innerHTML = 'Second';
modalWindow2.buttons.innerHTML = '<button id="close-2-js">CLOSE</button>';

let open1Button = document.getElementById('open-1-js');
let open2Button = document.getElementById('open-2-js');
let open2aButton = document.getElementById('open-2a-js');
let close1Button = document.getElementById('close-1-js');
let close2Button = document.getElementById('close-2-js');
let textElement = document.getElementById('text-js');

open1Button.addEventListener('click', (event) => {
    modalWindow1.open(event.currentTarget);
});

open2Button.addEventListener('click', (event) => {
    modalWindow2.open(event.currentTarget);
});

open2aButton.addEventListener('click', () => {
    modalWindow2.open();
});

close1Button.addEventListener('click', () => {
    modalWindow1.close();
});

close2Button.addEventListener('click', () => {
    modalWindow2.close();
});

modalWindow1.addEventListener(ModalWindowViewEvent.OPEN_EVENT, onEvent);
modalWindow1.addEventListener(ModalWindowViewEvent.CLOSE_EVENT, onEvent);
modalWindow1.addEventListener(ModalWindowViewEvent.OPEN_ANIMATION_END_EVENT, onEvent);
modalWindow1.addEventListener(ModalWindowViewEvent.CLOSE_ANIMATION_END_EVENT, onEvent);

function onEvent(event) {
    textElement.innerText = event.type;
}