import ModalWindowView from './src/ModalWindowView';
import ModalWindowViewEvent from './src/events/ModalWindowViewEvent';

export {ModalWindowView as default};
export {ModalWindowView, ModalWindowViewEvent};