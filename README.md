# README

## Installation
```bash
cd /path/to/project
yarn install
```

## Build demo
```bash
yarn webpack
```

## Use
```js
import {ModalWindowView, ModalWindowViewEvent} from '@casino/modal-window';

let modalWindow = new ModalWindowView();
modalWindow.content.style.lineHeight = '1.5';
modalWindow.content.innerHTML = 'First';
modalWindow.buttons.innerHTML = '<button id="close-1-js">CLOSE</button>';
modalWindow.open();

modalWindow.addEventListener(ModalWindowViewEvent.OPEN_EVENT, onEvent);
modalWindow.addEventListener(ModalWindowViewEvent.CLOSE_EVENT, onEvent);
modalWindow.addEventListener(ModalWindowViewEvent.OPEN_ANIMATION_END_EVENT, onEvent);
modalWindow.addEventListener(ModalWindowViewEvent.CLOSE_ANIMATION_END_EVENT, onEvent);

function onEvent(event) {
    console.log(event);
}
```